extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var message_target = null
var hide_timer = null


# Called when the node enters the scene tree for the first time.
func _ready():
	hide()
	$HideTimer.connect("timeout", self, "_on_hide_timeout")
	#show_message("Avoid the boulders!", 2)

func show_message(text, duration):
	$Message.text = text
	self.show()
	$HideTimer.wait_time = duration
	$HideTimer.start()

func _on_hide_timeout():
	self.hide()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
