extends Control

var final_score = null

func _ready():
	pass

func set_score(score):
	$ScoreLabel.text = "Final score: " + String(score)
	final_score = score
	# Global.new_high_score("Random gamer", score)

func submit_button_pressed():
	$SubmitPopup.popup()
	$SubmitButton.queue_free()

func submit_ok():
	var name = $SubmitPopup/NameEdit.text
	print("Name: " + name)
	if name != "" && final_score != null:
		print("Sending")
		Global.new_high_score(name, final_score)
	print("Sent")
	$SubmitPopup.hide()

func submit_cancel():
	 $SubmitPopup.hide()

func game_continue():
	get_tree().reload_current_scene()
