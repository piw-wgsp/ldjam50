extends AudioStreamPlayer

var repeat = true

func _ready():
	playing = not Global.DEBUG_RUN

func _process(delta):
	if Input.is_action_just_pressed("mute"):
		playing = not playing
		repeat = playing

func _on_MusicPlayer_finished():
	if repeat:
		playing = true
