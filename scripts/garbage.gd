extends KinematicBody
class_name Garbage

const TYPES = ["bluza", "ramen"]

var rng = RandomNumberGenerator.new()
var speed = Vector3(0, 0, 0)

func _ready():
	randomize_garbage()

func randomize_garbage():
	rng.randomize()
	var id = rng.randi_range(0, TYPES.size() - 1)
	$sprite.animation = TYPES[id]


#func _process(delta):
#	pass

func _physics_process(delta):
	speed += Vector3(0, -50, 0) * delta
	var collision = move_and_collide(speed * delta)


func _on_Area_body_entered(body):
	if body is Hero:
		body.went_into_garbage()


func _on_Area_body_exited(body):
	if body is Hero:
		body.left_the_garbage()
