extends KinematicBody
class_name Hero

const SPEED = 1000
const SPEED_IN_GARBAGE = 300
const UP = Vector3(0, 1, 0)
const NEXT_SPIN_LEVEL = 5
const SPIN_LIMIT = 5

var lives
var gravity
var in_garbage
var spins
var spin_exp

#var speed = Vector3(0, 0, 0)

var rng = RandomNumberGenerator.new()

func _ready():
	rng.randomize()
	lives = true
	gravity = Vector3(0, -300, 0)
	in_garbage = false
	spins = 0
	spin_exp = 0

func _process(delta):
	pass

func _physics_process(delta):
	var dir_vector = process_input_movement(delta)
	var speed = SPEED
	if in_garbage:
		speed = SPEED_IN_GARBAGE
	var move_vector = (dir_vector * speed + gravity) * delta
	
	var collision = move_and_collide(move_vector, true, true, true)
	if collision and collision.collider.is_class("Projectile"):
		collision.collider.touch_hero(self, Vector3(0, 0, 0))
		move_vector = collision.remainder
	
	move_and_slide(move_vector, UP)
	if is_on_floor():
		#print("on floor")
		pass
	else:
		pass

func process_input_movement(delta):
	var dir_vector = Vector3(0, 0, 0)
	
	if not lives:
		return dir_vector
		
	if Input.is_action_pressed("ui_left"):
		dir_vector += Vector3(-1, 0, 0)
	elif Input.is_action_pressed("ui_right"):
		dir_vector += Vector3(1, 0, 0)
	
	if Input.is_action_pressed("ui_up"):
		dir_vector += Vector3(0, 0, -1)
	elif Input.is_action_pressed("ui_down"):
		dir_vector += Vector3(0, 0, 1)
		
	if dir_vector != Vector3(0, 0, 0):
		dir_vector = dir_vector.normalized()
	
	if Input.is_action_just_pressed("action"):
		spin()
		
	if Input.is_action_just_pressed("jump"):
		spin()
		
	return dir_vector
	
func dead():
	$"..".game_dead()
	$sprite.frame = 2
	collision_layer = 0
	collision_mask = 0
	gravity = Vector3(0, -150, 60)
	$Tween.interpolate_property(self, "rotation", Vector3(-25, 0, 0), Vector3(-250, 0, 0), 100, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	$Tween.start()
	lives = false
	$"../Camera".dead_cam()
	$"../Camera/HUD".update_spins(0)
	

func spin():
	if spins > 0:
		spins -= 1
	else:
		return
	
	$"../Camera/HUD".update_spins(spins)
	$spin_sound.play()
	$Tween.interpolate_property(self, "rotation", Vector3(-25, 0, 0), Vector3(-25, PI*8, 0), 1, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	$Tween.start()
	var bodies = $SpinArea.get_overlapping_bodies()
	for body in bodies:
		throw_projectile_away_if_spinning(body)

func is_spinning():
	return $Tween.is_active()

func jump():
	if is_on_floor():
		#speed.y = 500
		pass

func play_random_beep():
	var which = rng.randi_range(1, 4)
	match which:
		1:
			$beep1.play()
		2:
			$beep2.play()
		3:
			$beep3.play()
		_:
			$beep4.play()

func play_pitched_beep():
	$beep3.pitch_scale = 1.0 + 0.05 * (spin_exp - 1)
	$beep3.play()

func add_score(value):
	get_parent().add_score(value)
	add_spin_exp()
	
func add_spin_exp():
	spin_exp += 1
	if spin_exp >= NEXT_SPIN_LEVEL:
		if spins >= SPIN_LIMIT:
			play_pitched_beep()
			spin_exp -= 1
			return
		spin_exp = 0
		spins += 1
		$beep2.play()
		$"../Camera/HUD".update_spins(spins)
	else:
		play_pitched_beep()

func throw_projectile_away_if_spinning(body):
	if not is_spinning():
		return
	if not body.is_class("Projectile"):
		return
	
	body.throw_away(translation)

func went_into_garbage():
	in_garbage = true

func left_the_garbage():
	in_garbage = false
