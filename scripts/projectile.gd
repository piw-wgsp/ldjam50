extends KinematicBody
class_name Projectile

const UP = Vector3(0, 1, 0)
const SPEED = 15
const GRAVITY = -30
const THROW_FORCE = 100
const DESTRUCTION_DISTANCE = 200
#const ROTATION_SPEED = SPEED/3.1415
const BAD_ITEMS = ["papec", "phone", "czapka", "ulotka"]
const GOOD_ITEMS = ["switch", "cd", "figurka", "erohon"]

var rng = RandomNumberGenerator.new()

var speed = Vector3(0, 0, 0)
var is_good = false
var thrown_away = false

func is_class(c):
	return c == "Projectile"

func _ready():
	pass

func _physics_process(delta):
	speed += Vector3(0, -50, 0) * delta
	var collision = move_and_collide(speed * delta)
	if collision:
		speed.y = 0
	if translation.length() > DESTRUCTION_DISTANCE:
		queue_free()
	if thrown_away:
		return
	collision = move_and_collide(Vector3(0, 0, SPEED) * delta)
	if collision:
		var other = collision.collider
		if other is Hero:
			touch_hero(other, collision.remainder)

func set_random_bad():
	rng.randomize()
	var id = rng.randi_range(0, BAD_ITEMS.size() - 1)
	$sprite.animation = BAD_ITEMS[id]
	is_good = false
	$sprite.modulate = Color.red
	_on_Tween_tween_completed(0, 0)
	
func set_random_good():
	rng.randomize()
	var id = rng.randi_range(0, GOOD_ITEMS.size() - 1)
	$sprite.animation = GOOD_ITEMS[id]
	is_good = true

func set_random():
	rng.randomize()
	if rng.randi() % 2 == 0:
		set_random_good()
	else:
		set_random_bad()

func touch_hero(hero, remainder):
	if is_good:
		hero.add_score(5)
		queue_free()
	else:
		hero.translation += remainder
		translation += remainder

func throw_away(center):
	if is_good:
		return
	collision_layer = 0
	collision_mask = 0
	var dir = (translation - center).normalized()
	speed = dir * THROW_FORCE + Vector3(0, THROW_FORCE, 0)
	thrown_away = true

func _on_Tween_tween_completed(object, key):
	var start = Color.white
	var end = Color.red
	if $sprite.modulate.g < 0.5:
		start = Color.red
		end = Color.white
	$Tween.interpolate_property($sprite, "modulate", start, end, 0.25, Tween.TRANS_LINEAR)
	$Tween.start()
