extends Node

var DEBUG_RUN = OS.is_debug_build()
#var DEBUG_RUN = false

func _ready():
	pass

func new_high_score(player_name, score):
	SilentWolf.Scores.persist_score(player_name, score)
