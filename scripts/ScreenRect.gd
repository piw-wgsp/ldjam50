extends ColorRect

const BLACKOUT_SPEED = 0.7
const FADE_FULL = 1.5

var fade_speed

# Called when the node enters the scene tree for the first time.
func _ready():
	fade_speed = 0


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	color.a += delta * fade_speed
	#if fade_completed():
	#	get_tree().reload_current_scene()

func dead_anim():
	fade_speed = BLACKOUT_SPEED

func fade_completed():
	return color.a >= 1.5
