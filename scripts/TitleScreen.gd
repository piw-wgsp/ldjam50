extends Control

func _ready():
	if Global.DEBUG_RUN:
		_start_solo()

func _start_solo():
	var error = get_tree().change_scene("res://demo.tscn")
	if error != OK:
		print("Fatal error: " + String(error))

func _show_instruction():
	var error = get_tree().change_scene("res://instruction.tscn")
	if error != OK:
		print("Fatal error: " + String(error))
