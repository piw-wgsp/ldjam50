extends CanvasLayer


# Called when the node enters the scene tree for the first time.
func _ready():
	pass


func _process(delta):
	pass

func update_spins(spins):
	for n in $SpinContainer/VBoxContainer.get_children():
		$SpinContainer/VBoxContainer.remove_child(n)
		n.queue_free()
	
	while spins > 0:
		var rect = TextureRect.new()
		rect.texture = preload("res://res/spin_icon.png")
		rect.margin_top = spins * 100
		$SpinContainer/VBoxContainer.add_child(rect)
		spins -= 1
