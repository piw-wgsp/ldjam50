extends Camera


const DEAD_CAM_SPEED = Vector3(0, -5, 2)

var speed = Vector3(0, 0, 0)


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	translate(speed*delta)

func dead_cam():
	speed = DEAD_CAM_SPEED
	$HUD/ScreenRect.dead_anim()
