extends Timer


const PROJECTILE = preload("res://actors/projectile.tscn")
const GARBAGE = preload("res://actors/garbage.tscn")
const START = -6
const RANGE = -40
const MIN_DIST = 2.5

var rng = RandomNumberGenerator.new()
var level = 0
var to_next_level = 10
var garbage_positions = [
	Vector3(-6, 100, -24),
	Vector3(-2, 100, -15),
	Vector3(-5, 100, -3),
	Vector3(6, 100, -25),
	Vector3(4, 100, -15),
	Vector3(6, 100, -5),
	Vector3(0, 100, -6),
	Vector3(2, 100, -12),
	Vector3(7, 100, -18),
	Vector3(-5, 100, -24),
]

func _ready():
	rng.randomize()
	garbage_positions.shuffle()

func _process(delta):
	pass
	
func _on_Timer_timeout():
	to_next_level -= 1
	if to_next_level <= 0:
		handle_level_progress()
	match level:
		0:
			generate_projectiles_row(0, 3, 1)
		1:
			generate_projectiles_row(0, 2, 0.5)
		2:
			generate_projectiles_row(0, 3, 0.4)
		3:
			generate_projectiles_row(0, 3, 0.3)
		4:
			generate_projectiles_row(1, 4, 0.3)
		5:
			generate_projectiles_row(2, 5, 0.0)
		6:
			generate_projectiles_row(3, 5, 0.2)
		7:
			generate_projectiles_row(3, 6, 0.5)
		8:
			generate_projectiles_row(3, 5, 0.0)
		9:
			generate_projectiles_row(3, 5, 0.1)
		10:
			generate_projectiles_row(3, 6, 0.3)
		11:
			generate_projectiles_row(4, 6, 0.0)
		12:
			generate_projectiles_row(3, 6, 0.1)
		13:
			generate_projectiles_row(4, 5, 0.3)
		14:
			generate_projectiles_row(0, 6, 0.2)
		15:
			generate_projectiles_row(0, 6, 0.0)
		15:
			generate_projectiles_row(3, 5, 0.2)
		_:
			generate_projectiles_row(4, 6, 0.15 - (level - 15) *0.02)

func handle_level_progress():
	level += 1
	to_next_level = level * 5
	new_garbage()
	match level:
		1:
			$"../AnimationPlayer".play("MomJumpIn")
		2:
			$"../AnimationPlayer".play("IdolJump1")
		3:
			$"../AnimationPlayer".play("MomJump1")
		4:
			$"../AnimationPlayer".play("IdolJump2")
		5:
			$"../AnimationPlayer".play("MomJump2")
		6:
			#$"../AnimationPlayer".play("IdolJumpOut")
			pass
		_:
			pass

func get_positions():
	var positions = [0, 1, 2, 3, 4, 5]
	positions.shuffle()
	return positions

func generate_projectiles_row(minimum, total, good_probability):
	var positions = get_positions()
	assert(total <= positions.size())
	
	var iBalls = rng.randi_range(minimum, total)
	for i in iBalls:
		var position = positions.pop_back()
		new_projectile(position * MIN_DIST, rng.randf_range(0, 1) <= good_probability)

func good_projectile(position):
	new_projectile(position * MIN_DIST, true)
	
func bad_projectile(position):
	new_projectile(position * MIN_DIST, false)

func new_projectile(position, is_good):
	var projectile = PROJECTILE.instance()
	get_parent().add_child(projectile)
	if is_good:
		projectile.set_random_good()
	else:
		projectile.set_random_bad()
	projectile.translation = Vector3(START + position, 20, RANGE)
	
func new_garbage():
	var garbage = GARBAGE.instance()
	garbage.translation = garbage_positions.pop_back()
	get_parent().add_child(garbage)
